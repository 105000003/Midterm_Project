# Software Studio 2018 Spring Midterm Project
## Topic
* Orange Chat (Chat room with additional functions)
* Key functions (add/delete)(基本功能)
    1. chat 有
    2. load message history 有
    3. chat with new user 有
* Other functions (add/delete)(增加的功能) 
    4. 創群組
    5. 群組聊天，私人聊天

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
# 簡介
* 這是個群組聊天的網頁，傳網址給想要加入的人，建立自己的聊天室，也有私下對話的功能。
* host on : https://ss105000003.firebaseapp.com/
* https://105000003.gitlab.io/Midterm_Project


## Index page:
# 外觀:
* css : 用w3.css寫，參考w3.css schools網頁設計。
* 漢堡gif是用photoshop做的，再用css animate


# 功能:
* 按log me in會到登入頁面

![image](截圖/Capture6.PNG)


## Login page:
# 外觀:
* 運用Template Creative Login Form : https://colorlib.com/wp/html5-and-css3-login-forms/ 加以變化

# 功能:
* 可以用創自己的帳號，或是用google帳號登入

![image](截圖/GIF.gif)

## Welcome page(Create chat/Enter chat):
# 外觀:
* 一樣運用Template Creative Login Form

# 功能:
* 可以創建自己的聊天室，或是加入已知的聊天室
* gif示範的是創建和加入聊天室的過程

# Enter Chat/Create Chat demo
![image](截圖/GIF3.gif)


## Navagate page:
# 外觀:
* w3.css，在該群組的成員會顯示在主畫面上，版面隨著加入的人變動
* chatroom 參考 friendly chat https://github.com/firebase/friendlychat-web 寫的
![image](截圖/Capture5.PNG)


# 功能:
* 點選名子下方的private chat可以與他進行一對一聊天
* 點選chatroom可以跟該群組所有成員對話


# 版面隨著加入人數變動
![image](截圖/Capture5.PNG)
# 群組對話
![image](截圖/Capture8.PNG)
# 私人對話
![image](截圖/Capture4.PNG)
# 和自己對話
![image](截圖/Capture7.PNG)

## Real time database
* 登入方式有兩種
![image](截圖/Capture.PNG)
* 資料庫嘗試不用巢狀結構的方式做，頂多三層來記錄user目前所在的聊天室和對話人uid
* 聊天室結構
![image](截圖/Capture1.PNG)
* 個人聊天室結構，會兩邊同時更新
![image](截圖/Capture2.PNG)

## Security Report (Optional)

* 在realtime database限制只有當下登入者可以改變資料的讀出寫入，cloud storage也有所限制
* 網頁傳遞資料(不同html間)不是用cookie或是url傳遞，而是在database裡去處理，不容易在頁面中被截取
* 登入者的密碼帳號由firebase authentication處理，連我都不知道使用者密碼為何，被駭機率低，除非剛好信箱密碼都依樣，或是使用者為登出。
