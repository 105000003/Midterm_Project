function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('email');
    var txtPassword = document.getElementById('password');
    var btnLogin = document.getElementById('login');//login

    var btnGoogle = document.getElementById('sign-in');//google

    var newEmail = document.getElementById('new_email');
    var newPassword = document.getElementById('new_password');
    var btnSignUp = document.getElementById('creatAcount');//signup

    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(function () {
                alert("Login success");
                window.location = "Welcome.html";
            })
            .catch(function (error) {
                // Handle Errors here.
                
                var errorCode = error.code;
                var errorMessage = error.message;
                alert(errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnGoogle.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
            .then(function () {
                window.location = "Welcome.html";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                alert(errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnSignUp.addEventListener('click', function () {
        var email = newEmail.value;
        var password = newPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(function () {
                alert("success ! ,You could sign in  right now!");
                newEmail.value = "";
                newPassword.value = "";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                alert(errorMessage);
                newEmail.value = "";
                newPassword.value = "";
            });
    });

}

window.onload = function () {
    initApp();
}

