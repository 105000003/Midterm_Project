//'use strict';

// Initializes FriendlyChat.
var Oname;

function FriendlyChat() {
    this.checkSetup();
    this.orangeName = document.getElementById('name');
    this.initFirebase();
    this.signOutButton = document.getElementById('sign-out');
    this.signOutButton.addEventListener('click', this.signOut.bind(this));

}

// Sets up shortcuts to Firebase features and initiate firebase auth.
FriendlyChat.prototype.initFirebase = function () {
    this.auth = firebase.auth();
    this.database = firebase.database();
    this.storage = firebase.storage();
    this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

FriendlyChat.MESSAGE_TEMPLATE =
    '<div class="message-container">' +
    '<div class="pic"><div>' +
    '<div class="name"></div>' +
    '<button class="btn btn-primary" onclick="private(this.id)">Private Chat to</button>'
'</div>';

FriendlyChat.prototype.onAuthStateChanged = function (user) {
    if (user) { // User is signed in!
        var link = user.uid;
        var ref = firebase.database().ref(link);
        ref.once("value")
            .then(function (snapshot) {
                Oname = snapshot.child("current").val();
            }).then(function () {
                var orangeName = document.getElementById('name');
                orangeName.innerHTML = Oname;
            }).then(function () {
                var link = Oname + '/' + 'members';
                var ref_room = firebase.database().ref(link);
                ref_room.off();
                var showUser = function (data) {
                    var val = data.val();
                    var div = document.getElementById(data.key);
                    if (!div) {
                        var container = document.createElement('div');
                        container.innerHTML = FriendlyChat.MESSAGE_TEMPLATE;
                        div = container.firstChild;
                        div.setAttribute('id', data.key);
                        //-----------------------------------------
                        var but = div.querySelector('.btn');
                        but.setAttribute('id', data.key);
                        //-----------------------------------------
                        var user_list = document.getElementById('user');//顯示照片
                        user_list.appendChild(div);
                    }
                    if (val.photoUrl) {
                        div.querySelector('.pic').style.backgroundImage = 'url(' + val.photoUrl + ')';
                    }
                    div.querySelector('.name').textContent = val.name;
                    setTimeout(function () { div.classList.add('visible') }, 1);
                }
                ref_room.limitToLast(12).on('child_added', showUser);
                ref_room.limitToLast(12).on('child_changed', showUser);
            })
    }
};
FriendlyChat.prototype.signOut = function () {
    this.auth.signOut();
    window.location = "index.html";
};
FriendlyChat.prototype.checkSetup = function () {
    if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
        window.alert('You have not configured and imported the Firebase SDK. ' +
            'Make sure you go through the codelab setup instructions and make ' +
            'sure you are running the codelab using `firebase serve`');
    }
};

function private(id) {
    var user = firebase.auth().currentUser;
    var link = user.uid + '/' + id;
    if (user.uid != id) {
        var ref = firebase.database().ref(link);
        ref.once("value")
            .then(function (snapshot) {
                var a = snapshot.exists();
                if (!a) { //DOESNT EXIST
                    ref.set('false'); //add chat space

                    var currentchat = firebase.database().ref(user.uid);//update current member of chat
                    currentchat.update({ chat: id });

                    var link_3 = id + '/' + user.uid; //add other member of chat
                    var oppochat = firebase.database().ref(link_3);
                    oppochat.once("value")
                        .then(function (snapshot) {
                            var b = snapshot.exists();
                            if (!b) {
                                oppochat.set('false');
                            }
                        });
                }
                else { //EXIST
                    alert("Load chat!");
                    var currentchat = firebase.database().ref(user.uid);//update current member of chat
                    currentchat.update({ chat: id });
                }
            }).then(function () {
                window.location = "private.html";
            });
    }
    else {
        alert("Don't talk to yourself! 就算很邊緣也別跟自己講話啦");
    }
}

window.onload = function () {
    window.friendlyChat = new FriendlyChat();
};
