'use strict';

// Initializes FriendlyChat.
function FriendlyChat() {
    this.checkSetup();

    // Shortcuts to DOM Elements.
    this.password = document.getElementById('password');//登入 
    this.orangeName = document.getElementById('orangename');
    this.passwordEnter = document.getElementById('passwordEnter');

    this.newPassword = document.getElementById('newPassword');//新聊天室
    this.newName = document.getElementById('newName');
    this.createPassword = document.getElementById('createPassword');

    this.user_name = document.getElementById('User');//顯示名子

    // Saves message on form submit.
    this.createPassword.addEventListener('click', this.CreatePassword.bind(this));
    this.passwordEnter.addEventListener('click', this.PasswordEnter.bind(this));
    this.initFirebase();
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
FriendlyChat.prototype.initFirebase = function () {
    this.auth = firebase.auth();
    this.database = firebase.database();
    this.storage = firebase.storage();
    this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

// Triggers when the auth state change for instance when the user signs-in or signs-out.
FriendlyChat.prototype.onAuthStateChanged = function (user) {
    if (user) { // User is signed in!
        var userName = user.displayName;   // Only change these two lines!
        this.user_name.innerHTML = userName;
    }
};

// Saves a new message on the Firebase DB.
FriendlyChat.prototype.PasswordEnter = function () { //enter orange
    var pass = this.password.value;
    var Oname = this.orangeName.value;
    //alert(Oname);
    alert('Searching for ' + Oname);
    var b = false;
    var rootRef = firebase.database().ref(Oname);
    rootRef.once("value")
        .then(function (snapshot) {
            b = snapshot.exists();
            if (b) {
                var childKey = snapshot.child("password").val();
                if (pass == childKey) {
                    var user = firebase.auth().currentUser;
                    var link = Oname + '/' + "members";
                    var usersRef = firebase.database().ref(link);
                    if (user) {
                        usersRef.child(user.uid).set({
                            name: user.displayName,
                            email: user.email,
                            photoUrl: user.photoURL || '/images/profile_placeholder.png'
                        });
                    }
                    //--------------------------------------------------------------
                    var ref = firebase.database().ref(user.uid);
                    ref.once("value")
                        .then(function (snapshot) {
                            var a = snapshot.exists();
                            if (!a) { //DOESNT EXIST
                                alert("New user!");
                                ref.set({
                                    current: Oname,
                                    chat:false
                                });
                            }
                            else { //EXIST
                                alert("Old User!")
                                var Ref = firebase.database().ref(user.uid);
                                Ref.update({current: Oname});
                            }
                        });
                    //--------------------------------------------------------------
                    alert("Orange entered!");

                    window.location = "Navagate.html";
                } else {
                    alert("Orange doesn't exist! Please create orange.");
                }

            }
        });
};

FriendlyChat.prototype.CreatePassword = function () {  //create orange
    var pass = this.newPassword.value;
    var orangename = this.newName.value;
    alert("pressEnter!");

    var rootRef = firebase.database().ref();
    var user = firebase.auth().currentUser;
    if (user) {
        rootRef.child(orangename).set({
            chat: false,
            members: false,
            name: orangename,
            password: pass
        });
    }
    alert('Added Orange!');
};

FriendlyChat.prototype.checkSignedInWithMessage = function () {
    if (this.auth.currentUser) {
        return true;
    }
};


FriendlyChat.prototype.checkSetup = function () {
    if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
        window.alert('You have not configured and imported the Firebase SDK. ' +
            'Make sure you go through the codelab setup instructions and make ' +
            'sure you are running the codelab using `firebase serve`');
    }
};

window.onload = function () {
    window.friendlyChat = new FriendlyChat();
};
