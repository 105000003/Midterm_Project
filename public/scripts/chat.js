'use strict';

//all variables
var messageList;
var messageForm;
var messageInput;
var submitButton;
var submitImageButton;
var imageForm;
var mediaCapture;
var signInSnackbar;
var LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif';
var MESSAGE_TEMPLATE =
  '<div class="message-container">' +
  '<div class="spacing"><div class="pic"></div></div>' +
  '<div class="message"></div>' +
  '<div class="name"></div>' +
  '</div>';



function initApp() {
  return new Promise(function (resolve, reject) {
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        var link = user.uid;
        var ref = firebase.database().ref(link);
        ref.once("value")
          .then(function (snapshot) {
            var Oname = snapshot.child("current").val();
            resolve(Oname);
          });
      } else {
        reject("User is not logged in")
      }
    });
  });
}
window.onload = function () {
  initApp().then(function (Oname) {
    // do things that depend on Oname
    checkSetup();
    messageList = document.getElementById('messages');
    messageForm = document.getElementById('message-form');
    messageInput = document.getElementById('message');
    submitButton = document.getElementById('submit');
    submitImageButton = document.getElementById('submitImage');
    imageForm = document.getElementById('image-form');
    mediaCapture = document.getElementById('mediaCapture');
    signInSnackbar = document.getElementById('must-signin-snackbar');

    messageForm.addEventListener('submit', saveMessage, false);
    messageForm.myParam = Oname;


    var buttonTogglingHandler = toggleButton;
    messageInput.addEventListener('keyup', buttonTogglingHandler);
    messageInput.addEventListener('change', buttonTogglingHandler);


    submitImageButton.addEventListener('click', function (e) {
      e.preventDefault();
      mediaCapture.click();
    });
    mediaCapture.addEventListener('change', saveImageMessage);
    mediaCapture.myParam = Oname;

    loadMessages(Oname);

  }).catch(function (error) {
    alert(error);
  });
};

// Loads chat messages history and listens for upcoming ones.
function loadMessages(Oname) {
  var link = Oname + '/' + 'chat';
  var messagesRef = firebase.database().ref(link);
  messagesRef.off();
  var setMessage = function (data) {
    var val = data.val();
    displayMessage(data.key, val.name, val.text, val.photoUrl, val.imageUrl);
  };
  messagesRef.limitToLast(12).on('child_added', setMessage);
  messagesRef.limitToLast(12).on('child_changed', setMessage);
};

// Saves a new message on the Firebase DB.
function saveMessage(evt) {
  var Oname = evt.target.myParam;
  if (messageInput.value) {
    var currentUser = firebase.auth().currentUser;
    var link = Oname + '/' + "chat";
    var messagesRef = firebase.database().ref(link);
    messagesRef.push({
      name: currentUser.displayName,
      text: messageInput.value,
      photoUrl: currentUser.photoURL || '/images/profile_placeholder.png'
    }).then(function () {
      resetMaterialTextfield(messageInput);
      toggleButton();
    }).catch(function (error) {
      console.error('Error writing new message to Firebase Database', error);
    });
  }
};

// Sets the URL of the given img element with the URL of the image stored in Cloud Storage.
function setImageUrl(imageUri, imgElement) {
  // If the image is a Cloud Storage URI we fetch the URL.
  if (imageUri.startsWith('gs://')) {
    imgElement.src = LOADING_IMAGE_URL; // Display a loading image first.
    firebase.storage().refFromURL(imageUri).getMetadata().then(function (metadata) {
      imgElement.src = metadata.downloadURLs[0];
    });
  } else {
    imgElement.src = imageUri;
  }
};

// Saves a new message containing an image URI in Firebase.
function saveImageMessage(evt) {
  var Oname = evt.target.myParam;
  var file = event.target.files[0];
  imageForm.reset();
  if (!file.type.match('image.*')) {
    var data = {
      message: 'You can only share images',
      timeout: 2000
    };
    signInSnackbar.MaterialSnackbar.showSnackbar(data);
    return;
  }
  if (Oname) {
    //loading icon that will get updated with the shared image.
    var currentUser = firebase.auth().currentUser;
    var link = Oname + '/' + "chat";
    var messagesRef = firebase.database().ref(link);
    messagesRef.push({
      name: currentUser.displayName,
      imageUrl: LOADING_IMAGE_URL,
      photoUrl: currentUser.photoURL || '/images/profile_placeholder.png'
    }).then(function (data) {

      // Upload the image to Cloud Storage.
      var filePath = currentUser.uid + '/' + data.key + '/' + file.name;
      return firebase.storage().ref(filePath).put(file).then(function (snapshot) {

        // Get the file's Storage URI and update the chat message placeholder.
        var fullPath = snapshot.metadata.fullPath;
        return data.update({ imageUrl: firebase.storage().ref(fullPath).toString() });
      });
    }).catch(function (error) {
      console.error('There was an error uploading a file to Cloud Storage:', error);
    });
  }
};

// Resets the given MaterialTextField.
function resetMaterialTextfield(element) {
  element.value = '';
  element.parentNode.MaterialTextfield.boundUpdateClassesHandler();
};

// Displays a Message in the UI.
function displayMessage(key, name, text, picUrl, imageUri) {
  var div = document.getElementById(key);
  // If an element for that message does not exists yet we create it.
  if (!div) {
    var container = document.createElement('div');
    container.innerHTML = MESSAGE_TEMPLATE;
    div = container.firstChild;
    div.setAttribute('id', key);
    messageList.appendChild(div);
  }
  if (picUrl) {
    div.querySelector('.pic').style.backgroundImage = 'url(' + picUrl + ')';
  }
  div.querySelector('.name').textContent = name;
  var messageElement = div.querySelector('.message');
  if (text) { // If the message is text.
    messageElement.textContent = text;
    messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
  } else if (imageUri) { // If the message is an image.
    var image = document.createElement('img');
    image.addEventListener('load', function () {
      messageList.scrollTop = messageList.scrollHeight;
    });
    setImageUrl(imageUri, image);
    messageElement.innerHTML = '';
    messageElement.appendChild(image);
  }
  // Show the card fading-in.
  setTimeout(function () { div.classList.add('visible') }, 1);
  messageList.scrollTop = messageList.scrollHeight;
  messageInput.focus();
};

function toggleButton() {
  var mi = document.getElementById('message');
  var sb = document.getElementById('submit');
  if (mi.value) {
    sb.removeAttribute('disabled');
  } else {
    sb.setAttribute('disabled', 'true');
  }
};

function checkSetup() {
  if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
    window.alert('You have not configured and imported the Firebase SDK. ' +
      'Make sure you go through the codelab setup instructions and make ' +
      'sure you are running the codelab using `firebase serve`');
  }
};